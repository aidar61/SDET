package com.digitalnomads.nambaTest;

import com.codeborne.selenide.CollectionCondition;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Slf4j
public class FoodTest extends BaseTest {
    @Test
    public void testFoodBtn(){
        log.warn("test food btn food test");
        homePage.openSite().clickFoodBtn().clickFirstElement();
    }
    @Test
    public void testFoodSize(){
        log.warn("test food size food test");
        homePage.openSite().clickFoodBtn();
        assertEquals(foodPage.foods.size(),16);
    }
    @Test
    public void testFoodContainsNationalKitchen(){
        homePage.openSite().clickFoodBtn();
        assertTrue(foodPage.foods.stream().anyMatch(e -> e.getText().contains("НАЦИОНАЛЬНАЯ КУХНЯ")));
        assertTrue(foodPage.foods.stream().anyMatch(e -> e.getText().contains("КИТАЙСКАЯ КУХНЯ")));
//        foodPage.foods.forEach(e -> System.out.println(e.getText()));
    }
    @Test
    public void selenideTest() throws InterruptedException {
        log.warn("selenide test food test");
        open("https://nambafood.kg/");
        $(By.xpath("//a[@class=\"menu-link \"][@href=\"/food\"]")).click();
        $$(By.xpath("//div[@class='cat-wrap']/a")).shouldHave(CollectionCondition.size(16));
        assertTrue($$(By.xpath("//div[@class='cat-wrap']/a")).stream().anyMatch(e -> e.getText().contains("НАЦИОНАЛЬНАЯ КУХНЯ")));
    }

}
