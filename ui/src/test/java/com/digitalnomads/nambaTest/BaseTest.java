package com.digitalnomads.nambaTest;

import com.codeborne.selenide.AssertionMode;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.testng.SoftAsserts;
import com.digitalnomads.helper.ElementActions;
import com.digitalnomads.pages.FoodPage;
import com.digitalnomads.pages.HomePage;
import com.digitalnomads.pages.TextBoxPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

@Listeners(SoftAsserts.class)
@Slf4j
public abstract class BaseTest {

    public ElementActions elementActions;
    public FoodPage foodPage;
    public HomePage homePage;
    TextBoxPage textBoxPage;

    @BeforeSuite(alwaysRun = true)
    public void setUpSelenide() {
        Configuration.headless = false;
        Configuration.remote = "http://localhost:4444/";
        Configuration.browser = "Chrome";
        Configuration.timeout = 10000;
        Configuration.screenshots = true;
        Configuration.browserSize = "1920x1080";
        Configuration.assertionMode = AssertionMode.SOFT;
        Configuration.pageLoadStrategy = "normal";
    }

    @BeforeClass(alwaysRun = true)
    public void setUpBrowser() {
        log.warn("========================+UI TEST IS STARTING+=======================");
        elementActions = new ElementActions();
        foodPage = new FoodPage();
        homePage = new HomePage();
        textBoxPage = new TextBoxPage();
    }

    @AfterSuite(alwaysRun = true)
    public void after() {
        log.warn("========================+UI TEST IS ENDING+=======================");
    }


}