package com.digitalnomads.nambaTest;

import com.digitalnomads.models.User;
import com.digitalnomads.utils.ModelManager;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Slf4j
public class DemoQATest extends BaseTest {
    @Test
    public void TextBoxTest(){
        log.warn("text box test demo qa test");
        User user = ModelManager.generateUser();
        textBoxPage.openTextBox()
                .fillUpTheForm(user).clickSubmit();
        assertTrue(textBoxPage.actualName.getText().contains(user.getFirstName()));
    }
}
