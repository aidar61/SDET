package com.digitalnomads.elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class EntryField extends BaseElement{
    public SelenideElement wtrn;

    public EntryField(String xpath) {
        this.wtrn = $(By.xpath(xpath));
    }

    public EntryField(String id,boolean isById) {
        this.wtrn = $(By.id(id));
    }
    public EntryField writeForm(String name){
        elementActions.writeText(wtrn,name);
        return this;
    }
}
