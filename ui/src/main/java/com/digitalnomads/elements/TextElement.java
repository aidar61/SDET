package com.digitalnomads.elements;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class TextElement extends BaseElement{
    public SelenideElement text;

    public TextElement(String xpath) {
        this.text = $(By.xpath(xpath));
    }
    public TextElement(String id,boolean isById) {
        this.text = $(By.id(id));
    }
    public String getText(){
        return text.getText();
    }
}
