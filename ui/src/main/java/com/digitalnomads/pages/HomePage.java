package com.digitalnomads.pages;

import com.digitalnomads.elements.Button;

import static com.codeborne.selenide.Selenide.open;

public class HomePage extends BasePage{
    public Button foodBtn = new Button("a","Еда");
    public Button aptekaBtn = new Button("a","Аптека");


    public HomePage openSite(){
        open("https://nambafood.kg/");
        return this;
    }
    public FoodPage clickFoodBtn(){
        foodBtn.click();
        return new FoodPage();
    }
}
