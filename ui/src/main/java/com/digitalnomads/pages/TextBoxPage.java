package com.digitalnomads.pages;

import com.digitalnomads.elements.Button;
import com.digitalnomads.elements.EntryField;
import com.digitalnomads.elements.TextElement;
import com.digitalnomads.models.User;

import static com.codeborne.selenide.Selenide.open;

public class TextBoxPage extends BasePage{
    public EntryField fullName = new EntryField("//input[@placeholder=\"Full Name\"]");
    public EntryField email = new EntryField("//input[@placeholder=\"name@example.com\"]");
    public EntryField currentAddress = new EntryField("//textarea[@placeholder=\"Current Address\"]");
    public EntryField permanentAddress = new EntryField("permanentAddress",true);
    public Button submitBtn = new Button("//button[@id=\"submit\"]");
    public TextElement actualName = new TextElement("//p[@id=\"name\"]");
    public TextElement actualEmail = new TextElement("//p[@id=\"email\"]");
    public TextElement actualCurrentAddress = new TextElement("currentAddress",true);
    public TextElement actualPermanentAddress = new TextElement("permanentAddress",true);

    public TextBoxPage openTextBox(){
        open("https://demoqa.com/text-box");
        return this;
    }
    public TextBoxPage enterName(String name){
        this.fullName.writeForm(name);
        return this;
    }
    public TextBoxPage enterEmail(String email){
        this.email.writeForm(email);
        return this;
    }
    public TextBoxPage enterCurrentAd(String address){
        currentAddress.writeForm(address);
        return this;
    }
    public TextBoxPage enterPermanentAd(String addrress){
        permanentAddress.writeForm(addrress);
        return this;
    }
    public TextBoxPage fillUpTheForm(User user){
        enterName(user.getFirstName() + " " + user.getLastName())
                .enterEmail(user.getEmail())
                .enterCurrentAd(user.getCurrentAddress())
                .enterPermanentAd(user.getPermanentAddress());
        return this;
    }
    public TextBoxPage clickSubmit(){
        submitBtn.click();
        return this;
    }
}
