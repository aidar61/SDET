package com.digitalnomads.pages;

import com.digitalnomads.helper.ElementActions;
import com.github.javafaker.Faker;

public abstract class BasePage {
    public ElementActions elementActions = new ElementActions();
    public Faker faker = new Faker();

}