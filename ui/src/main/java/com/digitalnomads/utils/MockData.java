package com.digitalnomads.utils;

import com.github.javafaker.Faker;

import java.util.Locale;

public class MockData {
    private static final Faker faker = new Faker(new Locale("EN"));

    public static String generateName() {
        return faker.name().firstName();
    }
    public static String generateLastName() {
        return faker.name().lastName();
    }
    public static String generateEmail() {
        return faker.internet().emailAddress();
    }
    public static String generateCurrentAddress() {
        return faker.address().fullAddress();
    }
    public static String generatePermanentAddress(){
        return faker.address().secondaryAddress();
    }
}
