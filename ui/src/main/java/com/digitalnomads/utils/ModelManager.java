package com.digitalnomads.utils;

import com.digitalnomads.models.User;

import static com.digitalnomads.utils.MockData.*;

public class ModelManager {
    public static User generateUser(){
        return User.builder()
                .firstName(generateName())
                .lastName(generateLastName())
                .email(generateEmail())
                .currentAddress(generateCurrentAddress())
                .permanentAddress(generatePermanentAddress())
                .build();
    }
}
