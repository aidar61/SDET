package com.digitalnomads.api.controllers;

import com.digitalnomads.api.ApiRequest;
import com.digitalnomads.api.entities.Course;
import com.digitalnomads.api.entities.User;
import com.digitalnomads.api.utils.JacksonUtils;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

import static com.digitalnomads.api.application.TalentLMSBaseEndpoints.*;
import static com.digitalnomads.api.application.TalentLMSBaseEndpoints.SIGN_UP;

@Slf4j
public class CourseController extends ApiRequest {
    public CourseController(String url) {
        super(url, API_KEY);
    }

    public Response getAllCourses() {
        return this.response = super.get(getEndpoint(API, V1, COURSES));
    }
    public Response getCourseById(String id){
        return super.getWithParams(getEndpoint(API,V1,COURSES),generateParams("id",id));
    }
    public Course createCourse(Course course){
        String jsonCourse = JacksonUtils.fromObjectToJson(course);
        return super.post(getEndpoint(API,V1,CREATECOURSE),jsonCourse).as(Course.class);
    }
    public void deleteCourse(String id){
        super.postWithParams(getEndpoint(API,V1,DELETECOURSE),new HashMap<>(){{
            put("course_id",id);
            put("deleted_by_user_id","1");
        }});
    }
    public boolean isCourseFull(){
        Course[] course = getAllCourses().as(Course[].class);
        log.info("Course lenth is {}", course.length);
        if (course.length >= 5) return true;
        return false;
    }
    public Response addUserToCourse(User user,Course course){
        return super.postWithParams(getEndpoint(API,V1,ADDUSERTOCURSE),new HashMap<>(){{
            put("user_id",user.getId());
            put("course_id",course.getId());
            put("role","learner");
        }});
    }
    public Response deleteUserFromCourse(String userId, String courseId){
        return super.getWithParams(getEndpoint(API,V1,"removeuserfromcourse"),new HashMap<>(){{
            put("user_id",userId);
            put("course_id",courseId);
        }});
    }

}
