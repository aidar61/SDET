package com.digitalnomads.api.asserts;

import com.digitalnomads.api.entities.Course;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;

@Slf4j
public class CourseAssert {
    Response response;
    public CourseAssert(Response response) {
        this.response = response;
    }
    public static CourseAssert assertThat(Response response){
        return new CourseAssert(response);
    }
    public CourseAssert isEquals(Course exCourse){
        Course actualCourse = this.response.as(Course.class);
        actualCourse.isEquals(exCourse);
        return this;
    }
    public CourseAssert isIdNotNull() {
        Course actualCourse = this.response.as(Course.class);
        Assertions.assertThat(actualCourse.getId()).isNotNull();
        log.info("ID {} of user not NULL", actualCourse.getId());
        return this;
    }
}