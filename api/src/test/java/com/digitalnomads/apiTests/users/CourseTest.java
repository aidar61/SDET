package com.digitalnomads.apiTests.users;

import com.digitalnomads.api.asserts.ApiAssert;
import com.digitalnomads.api.entities.Course;
import com.digitalnomads.api.entities.User;
import com.digitalnomads.api.utils.EntityManager;
import com.digitalnomads.apiTests.BaseApiTest;
import io.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CourseTest extends BaseApiTest {
    @Test
    public void getTest(){
        Course[] courses = courseController.getAllCourses().as(Course[].class);
        System.out.println(courses);
    }
    @Test
    public void getByIdTest(){
        Course course = courseController.getCourseById("126").as(Course.class);
        assertEquals(course.getName(),"api course");
    }
    @Test
    public void createCourseTest(){
        if (courseController.isCourseFull()){
            Course[] course = courseController.getAllCourses().as(Course[].class);
            courseController.deleteCourse(course[1].getId());
        }
        Course expectedCourse = EntityManager.generateCourse();
        courseController.createCourse(expectedCourse);

        ApiAssert.assertThat(courseController.getResponse())
                .isCorrectStatusCode(200)
                .assertCourse()
                .isEquals(expectedCourse)
                .isIdNotNull();

    }
    @Test
    public void addUserToCourseTest(){
        User user = userController.getAllUsers().as(User[].class)[1];
        Course course = courseController.getAllCourses().as(Course[].class)[1];
        courseController.addUserToCourse(user,course);
        ApiAssert.assertThat(courseController.getResponse())
                .isCorrectStatusCode(200);
        String exId = courseController.getResponse().jsonPath().getString("user_id");
        String exIdOfCourse = courseController.getResponse().jsonPath().getString("course_id");
        assertEquals("["+user.getId()+"]",exId);
        assertEquals("["+course.getId()+"]",exIdOfCourse);
    }

    @Test
    public void deleteUserFromCourseTest(){
        courseController.deleteUserFromCourse("21","129");

    }

}
